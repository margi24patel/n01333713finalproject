﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestProject
{
    public partial class AddNewPage : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO My_Page_Database" +
            "(pageid,pagetitle,pagecontent,pageauthor,pagepublishdate) VALUES";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddPage(object sender, EventArgs e)
        {
            //first get inputs from Add New Page 
            string title = txtAddPageTitle.Text.ToString();
            string paragraph = txtAddPageParagraph.Text.ToString();
            string selectAuthor = page_author.SelectedValue.ToString();
            string publishdate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            addquery += "((select max(pageid) FROM My_Page_Database)+1,'"+
              title+"','"+paragraph+ "','" + selectAuthor + "','" + publishdate + "'  )";


            debug.InnerHtml = addquery;

            insert_page.InsertCommand = addquery;
            insert_page.Insert();

        }
    }
}