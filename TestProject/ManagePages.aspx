﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="TestProject._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    

    <h2>Manage Pages</h2>
    
    <%--  i added search bar here--%>
        <div id="search_section">
        <asp:TextBox runat="server" ID="pagetitle_key"></asp:TextBox>
        <asp:Button runat="server" Text="Search" OnClick="Search_PageTitle" />
        </div>

    <asp:SqlDataSource runat="server" ID="page_select" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
    </asp:SqlDataSource>


    
     <%-- <div id="debug" class="querybox" runat="server">--%>





     <%--i added usercontrol here--%>
   <%-- <uctrlm:usercontrols ID="page_menulink" runat="server"></uctrlm:usercontrols>--%>
        <uctrl:usercontrol id="page_pick" runat="server" ></uctrl:usercontrol>
      
    <%--i added here button for addnew page--%>
    <a href="../AddNewPage.aspx">Add New Page</a>

        <asp:SqlDataSource runat="server" ID="page_pick_list" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
        </asp:SqlDataSource>

        <div id="page_query" runat="server" class="querybox">
        </div>

        <div id="debug" runat="server"></div>
        
    <asp:DataGrid ID="page_list" runat="server">
    </asp:DataGrid>
    

        </asp:Content>
