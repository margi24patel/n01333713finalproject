﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestProject
{
    public partial class Page : System.Web.UI.Page
    {
        private string sqlquery = "SELECT pageid,pagetitle as 'Title',pagecontent FROM My_Page_Database";
        //pageid to use for entire page
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
           //show page Details
        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null) page_title.InnerHtml = "No Pages Found";
            else
            {
                sqlquery += " WHERE PAGEID = " + pageid;

                page_content.SelectCommand = sqlquery;
                query_selection.InnerHtml = sqlquery;

                del_debug.InnerHtml = sqlquery;

                
                DataView pagetitleview = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
                DataRowView pagerowview = pagetitleview[0];
                string pagetitle = pagetitleview[0]["Title"].ToString();
                page_title.InnerHtml = pagetitle;


                DataView pagecontentview = (DataView)page_content.Select(DataSourceSelectArguments.Empty);
                DataRowView pagecontentrowview = pagecontentview[0];
                string pagecontent = pagecontentview[0]["pagecontent"].ToString();
                page_detail.InnerHtml = pagecontent;
                
            }


        }
        //Delete Pages
        protected void DelPage(object sender, EventArgs e)
        {
            string delquery = "DELETE FROM My_Page_Database WHERE pageid=" + pageid;

            del_debug.InnerHtml = delquery;
            del_page.DeleteCommand = delquery;
            del_page.Delete();
        }
    }
}