﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNewPage.aspx.cs" Inherits="TestProject.AddNewPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

           <%-- i added here--%>

            <h2>Add New Page</h2>

            <asp:SqlDataSource runat="server" ID="insert_page" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
            </asp:SqlDataSource>

            
            <div class="input row">
            <asp:Label runat="server" Text="PageTitle :"></asp:Label>
            <asp:TextBox runat="server" ID="txtAddPageTitle" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="requiredAddPageTitle" ControlToValidate="txtAddPageTitle" ErrorMessage="Please Enter a PageTitle"></asp:RequiredFieldValidator>
            
            </div>
            <br />
            
            <div class="input row">
            <asp:Label runat="server" Text="PageParagraph: "></asp:Label>
            <asp:TextBox runat="server" ID="txtAddPageParagraph"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="requiredAddParagraph" ControlToValidate="txtAddPageParagraph" ErrorMessage="Please Enter Page Description"></asp:RequiredFieldValidator>
            </div>
            <br />

            <div class="input row">
                <asp:Label runat="server" Text="Page Author: "></asp:Label>
                <asp:DropDownList ID="page_author" runat="server">
                    <asp:ListItem>Krish</asp:ListItem>
                     <asp:ListItem>Margi</asp:ListItem>
                     <asp:ListItem>Vishal</asp:ListItem>
                     <asp:ListItem>Mishwa</asp:ListItem>
                </asp:DropDownList>
            </div>
            <br />
            <asp:Button Text="Add Page" runat="server" OnClick="AddPage"/> 
            <br />
            <div runat="server" id="debug" class="querybox"></div>

        </div>
    </form>
</body>
</html>
