﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="TestProject.Page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           
          
           <%-- View of Pages--%>
  <h1 id="page_title" runat="server"></h1>

  <div id="query_selection" runat="server"></div>
  <br />

  <asp:SqlDataSource  ID="page_content" runat="server" ConnectionString="<%$ ConnectionStrings:page_sql_con %>"></asp:SqlDataSource> 

  <div id="page_detail" runat="server"></div>
  <br />
  <div id="debug" class="querybox" runat="server">
      <%--edit page here--%>
  <a href="EditPage.aspx?Pageid=<%Response.Write(this.pageid);%>">Edit</a>
  </div>
  <br />
              <%--delete pages--%>

  <asp:SqlDataSource runat="server" ID="del_page" ConnectionString="<%$ ConnectionStrings:page_sql_con %>">
  </asp:SqlDataSource>
  <br />

  <asp:Button runat="server" ID="del_page_btn" OnClick="DelPage" OnClientClick="if(!confirm('Are You Sure?')) return false;" Text="Delete" />
  <br />
  <div id="del_debug" class="querybox" runat="server">
  </div>
            
             
        </div>
    </form>
</body>
</html>
