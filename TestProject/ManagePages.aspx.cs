﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestProject
{
    public partial class _Default : Page
    {
        //pageid to use for entire page
        private string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        private string sqlquery = "SELECT pageid,pagetitle as 'Title' ,pageauthor as 'Page AuthorName',pagepublishdate as 'Publish Date' FROM My_Page_Database";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_pick_list.SelectCommand = sqlquery;
            page_query.InnerHtml = sqlquery;
            page_list.DataSource = Pages_Manual_Bind(page_pick_list);
            page_list.DataBind();

        }
        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;

            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            foreach (DataRow row in mytbl.Rows)
            {
                row["Title"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["Title"]

                    + "</a>";
                row["Page AuthorName"] = row["Page AuthorName"];
                row["Publish Date"] =row["Publish Date"];
            }

            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;
            return myview;
        }


        protected void Search_PageTitle(object sender,EventArgs e)
        {
            string newsql = sqlquery + " WHERE (1=1) ";
            string key = pagetitle_key.Text;

            if(key != "")
            {
                newsql +=
                    " AND ( pagetitle LIKE '%" + key + "%') ";
            }

            page_select.SelectCommand = newsql;
            page_query.InnerHtml = newsql;

            page_list.DataSource = Pages_Manual_Bind(page_select);

            page_list.DataBind();
        }
    }
}