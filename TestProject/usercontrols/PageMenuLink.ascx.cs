﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestProject.usercontrols
{
    public partial class PageMenuLink : System.Web.UI.UserControl
    {
        private string sqlquery = "SELECT pageid,pagetitle FROM My_Page_Database";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_menu_list.SelectCommand = sqlquery;
            string new_page_menu_link = Page_Menu_Link_ManualBind(page_menu_list);
            page_menu_link.InnerHtml = new_page_menu_link;

           // Page_Menu_Link_ManualBind(page_menu_list, "page_menu_link");
        }

        private string Page_Menu_Link_ManualBind(SqlDataSource src)
        {
            DataView pageview = (DataView)src.Select(DataSourceSelectArguments.Empty);
            string data = string.Empty;

            foreach(DataRowView row in pageview)
            {
                string page_link = "<li><a href=Page.aspx?pageid=" + row["pageid"] + ">" + row["pagetitle"].ToString() + "</a></li>";
                data += page_link;
            }
            return data;
        }

        //void Page_Menu_Link_ManualBind(SqlDataSource src, string menu_link)
        //{
        //    Menu page_menu_list = (Menu)FindControl(menu_link);

        //    DataView pageview = (DataView)src.Select(DataSourceSelectArguments.Empty);

        //    foreach(DataRowView row in pageview)
        //    {
        //        MenuItem page_menu_item = new MenuItem();
        //        page_menu_item.Text = row["pagetitle"].ToString();
        //        page_menu_item.Value = row["pageid"].ToString();
        //        page_menu_list.Items.Add(page_menu_item);
        //    }
        //}

    }
}