﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestProject
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get
            {
                return Convert.ToInt32(Request.QueryString["pageid"]);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Edit_Page()
        {

        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);

            DataRowView pagerow = getPageInfo(pageid);
            if(pagerow == null)
            {
                page_fullname.InnerHtml = "No Page Found";
                return;
            }

            
            txtEditPageTitle.Text = pagerow["pagetitle"].ToString();
            txtEditPageParagraph.Text = pagerow["pagecontent"].ToString();
            //i added for page author
            //edit_debug.InnerHtml = pagerow["pageauthor"].ToString().ToUpper(); 
            page_author.SelectedValue = pagerow["pageauthor"].ToString().ToUpper();
            
        }

        protected void Edit_Page(object sender,EventArgs e)
        {
            string title = txtEditPageTitle.Text.ToString();
            string paragraph = txtEditPageParagraph.Text.ToString();
            string pageauthor = page_author.SelectedValue;
            //string pageauthor = page_author.Text.ToString();

            string editquery = "Update My_Page_Database set pagetitle = '" + title +"'," +
                " pagecontent = '" + paragraph + "'," +
                 " pageauthor = '" + pageauthor + "'" +            //i added here

                " WHERE PAGEID = " + pageid;

            edit_debug.InnerHtml = editquery;

            edit_page.UpdateCommand = editquery;
            edit_page.Update();

        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "SELECT * FROM My_Page_Database WHERE pageid=" + pageid.ToString();
            page_select.SelectCommand = query;

            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            if(pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }
    }
}