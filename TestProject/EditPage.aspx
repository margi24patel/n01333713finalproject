﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="TestProject.EditPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <h2 runat="server" id="page_fullname">Edit Page</h2>
            <asp:SqlDataSource runat="server" ID="edit_page" ConnectionString="<%$ ConnectionStrings:page_sql_con %>"></asp:SqlDataSource>
            
            <asp:SqlDataSource runat="server" ID="page_select" ConnectionString="<%$ ConnectionStrings:page_sql_con %>"></asp:SqlDataSource>

            <div class="inputrow">
            <asp:Label runat="server" Text="PageTitle :"></asp:Label>
            <asp:TextBox runat="server" ID="txtEditPageTitle" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="requiredEditPageTitle" ControlToValidate="txtEditPageTitle" ErrorMessage="Please Enter a PageTitle"></asp:RequiredFieldValidator>
            </div>

            <div class="input row">
            <asp:Label runat="server" Text="PageParagraph: "></asp:Label>
            <asp:TextBox runat="server" ID="txtEditPageParagraph"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="requiredEditPageParagraph" ControlToValidate="txtEditPageParagraph" ErrorMessage="Please Enter Page Description"></asp:RequiredFieldValidator>
            </div>
            <br />
             

            <%--here i added here list of publishauthor using dropdownlist.--%>
            <%--i want to show list of selected author thta's why i have used dropdownlist here--%>
            <div class="input row">
                <asp:Label runat="server" Text="Page Author: "></asp:Label>
                <asp:DropDownList ID="page_author" runat="server">
                    <asp:ListItem>Krish</asp:ListItem>
                     <asp:ListItem>Margi</asp:ListItem>
                     <asp:ListItem>Vishal</asp:ListItem>
                     <asp:ListItem>Mishwa </asp:ListItem>
                </asp:DropDownList>
            </div>
            <br />

            <asp:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/> 
            <br />
            <div runat="server" id="edit_debug" class="querybox"></div>


        </div>
    </form>
</body>
</html>
